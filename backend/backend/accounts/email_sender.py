import os

from django.contrib.auth import get_user_model
from django.core.mail import send_mail, EmailMultiAlternatives
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.utils.html import strip_tags

UserModel = get_user_model()


# def email_sender(request, email):
#     user = UserModel.objects.get(email=email)
#     application_name = 'TestApplicationName'
#     subject = f'{application_name} account activation'
#     domain = "http://127.0.0.1:8000"
#     message = f"""Dear {user.username},
#
# Your {application_name} account has been created, please click on the URL below to activate it:
#
# {domain}/account/activate/?key={user.activation_key}
#
# Regards,
# {application_name} Team."""
#
#     from_email = os.environ.get('EMAIL_HOST_USER')
#
#     recipient_list = [email]
#     send_mail(subject,
#               message,
#               from_email,
#               recipient_list,
#               fail_silently=False)


def email_sender(request, email):
    user = UserModel.objects.get(email=email)
    application_name = 'TestApplicationName'
    subject = f'{application_name} account activation'
    domain = "http://127.0.0.1:8000"
    context = {
        'user': user,
        'application_name': application_name,
        'domain': domain
    }

    html_message = render_to_string('activation_email.html', context)

    plain_message = strip_tags(html_message)

    from_email = os.environ.get('EMAIL_HOST_USER')
    recipient_list = [email]

    msg = EmailMultiAlternatives(subject, plain_message, from_email, recipient_list)
    msg.attach_alternative(html_message, "text/html")
    msg.send(fail_silently=False)
