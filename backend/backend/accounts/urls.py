from django.urls import path

from backend.accounts.views import test_view, RegistrationView, registration, user_login, LoginView, \
    ProfileActivationAPIView

urlpatterns = [
    path('api/register/', RegistrationView.as_view(), name='user-registration'),
    path('api/login/', LoginView.as_view(), name='user-login'),
    path('account/activate/', ProfileActivationAPIView.as_view(), name='account-activation'),

    # tests
    path('', test_view, name='test_view'),
    path('registration/', registration, name='registration'),
    # path('login/', user_login, name='login'),
]
