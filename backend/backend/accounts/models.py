from django.db import models
from django.contrib.auth import models as auth
from backend.accounts.string_generator import random_string


# Create your models here.
class JHIUser(auth.AbstractUser):
    LANG_CHOICES = (
        ('en', 'English'),
        ('bg', 'Bulgarian'),
        ('fr', 'French'),
        ('de', 'German'),
    )

    id = models.BigAutoField(
        primary_key=True
    )
    username = models.CharField(
        max_length=50,
        unique=True
    )
    password = models.CharField(
        max_length=100,
        blank=True,
        null=True
    )
    first_name = models.CharField(
        max_length=50,
        blank=True,
        null=True
    )
    last_name = models.CharField(
        max_length=50,
        blank=True,
        null=True
    )
    email = models.EmailField(
        max_length=191,
        unique=True
    )
    image_url = models.CharField(
        max_length=255,
        blank=True,
        null=True
    )
    is_active = models.BooleanField(
        default=False
    )
    lang_key = models.CharField(
        max_length=10,
        choices=LANG_CHOICES,
        blank=True,
        null=True
    )
    activation_key = models.CharField(
        max_length=20,
        blank=True,
        null=True,
        unique=True,
    )
    reset_key = models.CharField(
        max_length=20,
        blank=True,
        null=True
    )
    reset_date = models.DateTimeField(
        blank=True,
        null=True
    )
    created_by = models.CharField(
        max_length=50,
        default="anonymousUser"
    )
    created_date = models.DateTimeField(
        auto_now_add=True,
        blank=True,
        null=True
    )
    last_modified_by = models.CharField(
        max_length=50,
        default="anonymousUser"
    )
    last_modified_date = models.DateTimeField(
        blank=True,
        null=True
    )

    def save(self, *args, **kwargs):
        if not self.activation_key:
            while True:
                activation_key = random_string(20)
                if not JHIUser.objects.filter(activation_key=activation_key).exists():
                    self.activation_key = activation_key
                    break

        if not self.reset_key:
            while True:
                reset_key = random_string(20)
                if not JHIUser.objects.filter(reset_key=reset_key).exists():
                    self.reset_key = reset_key
                    break

        super(JHIUser, self).save(*args, **kwargs)
