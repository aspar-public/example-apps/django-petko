from django.contrib.auth import get_user_model, login, authenticate
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404

from rest_framework import status
from rest_framework.fields import CharField
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework.views import APIView

from backend.accounts.email_sender import email_sender
from backend.accounts.forms import TestForm, LoginForm
from backend.accounts.serializers import UserSerializer

UserModel = get_user_model()


class RegistrationView(APIView):
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            user_data = {
                'id': user.id,
                'username': user.username,
                'firstName': user.first_name,
                'lastName': user.last_name,
                'email': user.email,
                'imageUrl': user.image_url,
                'langKey': user.lang_key,
                'activated': user.is_active,
                'createdBy': user.created_by,
                'createdDate': user.created_date,
                'lastModifiedBy': user.last_modified_by,
                'lastModifiedDate': user.last_modified_date,
                # ?? 'authorities': user.authorities
            }
            # # email send
            email_sender(request, user.email)
            return Response(user_data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProfileActivationAPIView(APIView):
    def get(self, request):
        activation_key = request.query_params.get('key')
        if not activation_key:
            return Response({'error': 'Activation key is missing'}, status=status.HTTP_400_BAD_REQUEST)

        user = get_object_or_404(UserModel, activation_key=activation_key)

        user.is_active = True
        user.save()

        info = {'status': f'User {user.username} activated successfully'}
        return Response(info, status=status.HTTP_200_OK)


class LoginSerializer(Serializer):
    login = CharField()
    password = CharField(
        write_only=True,
        style={
            'input_type': 'password'
        }
    )


class LoginView(APIView):
    serializer_class = LoginSerializer

    def post(self, request):
        login_field = request.data.get('login')
        password = request.data.get('password')
        user = authenticate(request, username=login_field, password=password)

        if user is None:
            try:
                user = UserModel.objects.get(email=login_field)
            except UserModel.DoesNotExist:
                user = None

        if user is not None and user.check_password(password):
            login(request, user)
            return Response(status=status.HTTP_200_OK)
        else:
            return Response({'detail': 'Invalid credentials'}, status=status.HTTP_401_UNAUTHORIZED)


# tests ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_view(request):
    form = TestForm()

    if request.method == 'POST':
        print(request.POST)
        a = TestForm(request.POST)
        if a.is_valid():
            a.save()
            print(a.data)
        else:
            print(a.errors)
    else:
        print("GET")
    return render(request, 'main.html', {"form": form})


def registration(request):
    form = TestForm()

    if request.method == 'POST':
        print(request.POST)
        a = TestForm(request.POST)
        if a.is_valid():
            mail = request.POST.get('email')
            print(mail)
            a.save()
            print(a.data)
            return redirect("check-email", email=mail)
        else:
            print(a.errors)
    else:
        print("GET")
    return render(request, 'registration.html', {"form": form})


def user_login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return HttpResponse("Login successful")
        else:
            return HttpResponse("Invalid login credentials")
    else:
        return render(request, 'login.html')  # You need to create a login.html template
