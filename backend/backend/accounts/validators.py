from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _

UserModel = get_user_model()


def validate_username(value):
    existing_user = UserModel.objects.filter(username=value).first()
    if existing_user:
        raise ValidationError(_("This username is already in use."))

    if len(value) < 5:
        raise ValidationError(_("Username must be at least 5 characters long."))


def validate_password_min(value):
    if len(value) < 8:
        raise ValidationError(_("Password must be at least 8 characters long."))


def validate_password_max(value):
    if len(value) > 60:
        raise ValidationError(_("Password must be at most 60 characters long."))


def validate_email(value):
    existing_user = UserModel.objects.filter(email=value).first()
    if existing_user:
        raise ValidationError(_("This email address is already in use."))
