from rest_framework import serializers

from backend.accounts.models import JHIUser
from backend.accounts.validators import validate_password_min, validate_username, validate_email, validate_password_max


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        write_only=True,
        validators=[validate_password_min, validate_password_max],
    )
    login = serializers.CharField(
        source='username',
        validators=[validate_username],
    )
    firstName = serializers.CharField(
        source='first_name',
        required=False,
        allow_blank=True
    )
    lastName = serializers.CharField(
        source='last_name',
        required=False,
        allow_blank=True
    )
    email = serializers.EmailField(
        validators=[validate_email],
    )
    imageUrl = serializers.CharField(
        source='image_url',
        required=False,
        allow_blank=True
    )
    langKey = serializers.CharField(
        source='lang_key',
        required=False,
        allow_blank=True
    )

    class Meta:
        model = JHIUser
        fields = [
            'login',
            'firstName',
            'lastName',
            'email',
            'imageUrl',
            'langKey',
            'password'
        ]

    def create(self, validated_data):
        user = JHIUser.objects.create_user(
            username=validated_data['username'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            email=validated_data['email'],
            image_url=validated_data['image_url'],
            lang_key=validated_data['lang_key'],
            password=validated_data['password']
        )

        return user
