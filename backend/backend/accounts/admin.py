from django.contrib import admin

from backend.accounts.models import JHIUser


# Register your models here.
@admin.register(JHIUser)
class UserAdmin(admin.ModelAdmin):
    pass
